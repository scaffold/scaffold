all: *

install: all
	DIR="`sed 1q conf-prefix`"; \
	BINDIR="$$DIR"/bin; \
	mkdir -p "$$BINDIR"; \
	cp * "$$BINDIR"/*.$$$$ && mv -f "$$BINDIR"/*.$$$$ "$$BINDIR"/*; \
	true

*: load target-*
	./load *



compile: conf-cc warn-auto.sh
	( cat warn-auto.sh; \
	CC=`sed 1q conf-cc`; \
	echo exec $$CC -c '"$$@"' \
	) > compile
	chmod 0755 compile

load: conf-ld conf-libs warn-auto.sh
	( cat warn-auto.sh; \
	LD=`sed 1q conf-ld`; \
	LIBS=`sed 1q conf-libs`; \
	echo exec $$LD -o '"$$@"' $$LIBS \
	) > load
	chmod 0755 load

makelib: warn-auto.sh
	( cat warn-auto.sh; \
	echo rm -f '"$$1"'; \
	echo ar cr '"$$@"'; \
	echo ranlib '"$$1"'; \
	echo case $$? in; \
	echo '127)' exit 0 ';;'; \
	echo esac \
	) > makelib
	chmod 0755 makelib

dist: all package
	mkdir `cat package`
	cp `cat Manifest` `cat package`
	tar cfz `cat package`.tar.gz `cat package`
	rm -rf `cat package`

